# Arch con btrfs_raid 0 y snap-pac

# Verificar si hay conexión
ip link (la conexión enp#s# debe indicar UP)

# Si se desea hacer la instalación por SSH
pacman -Sy openssh <br />
systemctl enable --now sshd <br />
passwd <br />
ssh root@ip (usar **ip address** si es necesario determinar la ip) <br />

# Pasos iniciales
loadkeys XX (verificar la lista con <b>ls /usr/share/kbd/keymaps/**/*.map.gz</b>) <br />
ls /sys/firmware/efi/efivars (si da error está en BIOS) <br />
timedatectl set-ntp true (verificar con **timedatectl status**)<br />


# Particionado (/dev/sda tiene 20 GB y /dev/sdb 23 GB)

Crear particionado de la siguiente manera: <br />
/dev/sda1 y /dev/sdb1 (20 GB c/u para /) <br />
/dev/sdb2 (EFI) <br />
/dev/sdb3 (swap) <br />

mkfs.btrfs -d raid0 /dev/sda1 /dev/sdb1 <br />
mkfs.vfat -F32 /dev/sdb2 <br />
mkswap /dev/sdb3 <br />
swapon /dev/sdb3 <br />

mount /dev/sda1 /mnt  (supuestamente al montar un /dev/sdX de un btfrs multidispositivo, se montan todos. Además, **sda1 y sdb1 tienen el mismo UUID**)<br />
btrfs subvolume create /mnt/@ <br />
btrfs subvolume create /mnt/@home <br />
btrfs subvolume create /mnt/@var <br />
btrfs subvolume create /mnt/@snapshots <br />
umount /mnt <br />
mount -o noatime,compress=lzo,space_cache,subvol=@ /dev/sda1 /mnt <br />
mkdir -p /mnt/{boot,home,var,.snapshots} <br />
mount -o noatime,compress=lzo,space_cache,subvol=@home /dev/sda1 /mnt/home <br />
mount -o noatime,compress=lzo,space_cache,subvol=@var /dev/sda1 /mnt/var <br />
mount -o noatime,compress=lzo,space_cache,subvol=@snapshots /dev/sda1 /mnt/.snapshots <br />
mount /dev/sdb2 /mnt/boot <br />
pacstrap /mnt base linux linux-firmware nano snapper intel-ucode<br />
genfstab -U /mnt >> /mnt/etc/fstab

# Ok, cambiemos de root.
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/**Region/City** /etc/localtime <br />
hwclock --systohc <br />
nano /etc/locale.gen  <br />
locale-gen <br />
nano /etc/locale.conf (LANG=en_US.UTF-8) <br />
nano /etc/hostname  <br />
nano /etc/hosts <br />
passwd <br />
pacman -Syu grub-btrfs efibootmgr networkmanager bash-completion os-prober mtools dosfstools base-devel linux-headers reflector git<br />
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB <br />
grub-mkconfig -o /boot/grub/grub.cfg (se presentará un error de snapper, es esperado) <br />
useradd -mG wheel neon <br />
passwd neon <br />
EDITOR=nano visudo <br />
exit (para salir de **arch-chroot**)<br />
 <br />
umount -a <br />
reboot <br />
<br /> <br />

# Primer booteo del sistema instalado.
pacman -S xorg <br />
pacman -S gdm <br />
pacman -S gnome (e instalar paquetes a elección) <br />

# Habilitar snapshots
umount /.snapshots  <br />
rm -rf /.snapshots/ <br />
snapper -c root create-config / <br />
nano /etc/snapper/configs/root (agregar usuario y modificar valores para cleanup) <br />
<br />
limits for timeline cleanup<br />
TIMELINE_MIN_AGE="1800"<br />
TIMELINE_LIMIT_HOURLY="5"<br />
TIMELINE_LIMIT_DAILY="8"<br />
TIMELINE_LIMIT_WEEKLY="10"<br />
TIMELINE_LIMIT_MONTHLY="70"<br />
TIMELINE_LIMIT_YEARLY="0"<br />
<br />
<br />
chmod a+rx /.snapshots/ <br />
systemctl enable --now snapper-timeline.timer  <br />
systemctl enable --now snapper-cleanup.timer  <br />
systemctl enable --now grub-btrfs.path  <br />


# Primer snapshot
snapper -c root create -c timeline --description PrimerSnap <br />
snapper -c root list  <br />

git clone https://aur.archlinux.org/snapper-gui-git.git <br />
cd snapper-gui-git/ <br />
makepkg -si PKGBUILD  <br />

# Incluir /boot en snapshots
sudo nano /etc/pacman.d/hooks/50-bootbackup.hook <br/>
 <br/>
agregar lo siguiente:  <br/>
[Trigger] <br/>
Operation = Upgrade <br/>
Operation = Install <br/>
Operation = Remove <br/>
Type = Path <br/>
Target = usr/lib/modules/*/vmlinuz <br/>
 <br/>
[Action] <br/>
Depends = rsync <br/>
Description = Backing up /boot... <br/>
When = PreTransaction <br/>
Exec = /usr/bin/rsync -a --delete /boot /.bootbackup <br/>


# Spotify
curl -sS https://download.spotify.com/debian/pubkey_0D811D58.gpg | gpg --import - <br/>
git clone https://aur.archlinux.org/spotify.git<br/>
cd spotify<br />
makepkg -si PKGBUILD <br/>



 
